# Angelababy - A Conversational Agent
##Angelababy is a conversational agent developed by Group SexyKitty in UNSW
----
### What can she do?
+ Solve the questions about course information in UNSW(only CSE postgraduate courses at present)
+ Daily chat function
----
### Installation environment and startup commands
   + Online user
      + This program has been deployed on the Internet by heroku
	  + Everyone can chat with Agent at [Angelababy - A Conversational Agent](https://sexykitty.herokuapp.com/)
   + Local user
	  + The test environment is Window10/Linux + Python3.5
	  + Download the entire project and Change the directory under the folder named program
	  + Install all the required python packages by using the following command in the terminal
	  	+ `pip3 install -r requirements.txt`
	  + Change the directory under the folder named local
      + Run code by using the following command in the terminal
	  	+ `python app.py`
	  + Finally, open a browser and enter the website http://127.0.0.1:5000/
----
### User instructions
   + You will see some introductions on the website home page
   + If you want to chat with agnet, please click the button "Start chatting", then you can chat with agent happily!
   + If you do not know what questions can you ask to agent, please click the button "Chat tips"
   + If you want to clear all the conversation history, please click the button "Restart chatting"
----
-------------------
### Description of program code directory structure
#### There are four folders in the program directory
   + Folder named local
      + The file in this folder is used to run agent program locally
	  + Run the code as the instructions above(Local user)
	  + Chat with agent locally
   + Folder named online
      + Depoly local program to online website based on heroku API
   + Folder named web_crawler
      + The program here has the function of data update
	  + Run code by using the command `python web_crawler.py`, the most recent course information will be collected
	  + Course information is saved in csv file named "dataset.csv"
	  + Replace the file with the same name in another folder to complete the data update
   + Folder named cloud_database
      + Download and upload between the local database and cloud database