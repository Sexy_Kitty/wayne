# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 16:12:05 2018

@author: Paul
"""

import csv
from bson.objectid import ObjectId
from pymongo import MongoClient


def down_load_csv(port_num, DB_name, DB_host, DB_user, DB_password):
    client = MongoClient(host = DB_host, port = port_num)
    db = client[DB_name]
    db.authenticate(DB_user, DB_password)
    
    my_collection_1 = db['Collection']
    list_ = my_collection_1.find()
    
    row = []
    
    ##########collection_id change everytime
    collection_id = '5bc2e7c0ce13501860a0b5bc'
    object_id = ObjectId(collection_id)
    for i in list_:
        if i['_id'] == object_id:
            for j in range(len(i) - 1):
                cur = []
                
                temp = i['record' + str(j+1)]
#                for i in temp:
#                    print(temp[i])
                cur.append(temp['LECTURE NAME'])
                cur.append(temp['LECTURE CODE'])
                cur.append(temp['LECTURE LOCATION AND TIME'])
                cur.append(temp['PROFESSOR'])
                cur.append(temp['PROFESSOR CONTACT'])
                cur.append(temp['LECTURE CAREER'])
                cur.append(temp['LECTURE WEBSITE'])
                cur.append(temp['LECTURE UNITS OF CREDIT'])
                cur.append(temp['LECTURE INTRODUCTION'])
                row.append(cur)
    return row


def csv_file(rows):
    with open("dataset.csv","w", newline='') as csvfile: 
        writer = csv.writer(csvfile)
        writer.writerow(['LECTURE NAME', 'LECTURE CODE', 'LECTURE LOCATION AND TIME',
                    'LECTURE CAREER', 'LECTURE WEBSITE', 'LECTURE UNITS OF CREDIT',
                   'LECTURE INTRODUCTION','PROFESSOR', 'PROFESSOR CONTACT'])
        writer.writerows(rows)

port_number = 61062
db_name = 'my-database'
db_host = 'ds161062.mlab.com'
db_user = 'zhang666'
db_password = 'zh466503701'

row = down_load_csv(port_number, db_name, db_host, db_user, db_password)
## store in csv file
csv_file(row)





