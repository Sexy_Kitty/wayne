# -*- coding: utf-8 -*-
import aiml
import sys
import os
#current_path = os.getcwd()

def get_module_dir(name):
    #print("module", sys.modules[name])
    path = getattr(sys.modules[name], '__file__', None)
    #print(path)
    if not path:
        raise AttributeError('module %s has not attribute __file__' % name)
    return os.path.dirname(os.path.abspath(path))

alice_path = get_module_dir('aiml') + '/botdata/alice'

os.chdir(alice_path)        # change path to corpus direction

alice = aiml.Kernel()       # create robort Alice
alice.learn("startup.xml")  # upload ...\\botdata\\alice\\startup.xml
alice.respond('LOAD ALICE') # upload...\\botdata\\alice corpus
# add some personal information
alice.setBotPredicate("name","Angelababy")
alice.setBotPredicate("master","Sexy Kitty")
alice.setBotPredicate("botmaster","master")
alice.setBotPredicate("deveopers","Sexy Kitty")
alice.setBotPredicate("address","UNSW kensington")
alice.setBotPredicate("birthplace","UNSW kensington")
alice.setBotPredicate("birthday","01/08/2018")
alice.setBotPredicate("location","Australia")
alice.setBotPredicate("dailyclients","many")
alice.setBotPredicate("order","chatbot")
alice.setBotPredicate("famiy","chatbot")
alice.setBotPredicate("phylum","chatbot")
alice.setBotPredicate("gender","female")

def Alice_response(message):
    response = alice.respond(message)
    return response
