# -*- coding: utf-8 -*-
import pandas as pd
import random
from input_process import *
#from chat import *
import os

current_path = os.getcwd()
Data = current_path+'/dataset.csv'
columns = ['LECTURE NAME', 'LECTURE CODE', 'LECTURE LOCATION AND TIME',
           'PROFESSOR', 'PROFESSOR CONTACT', 'LECTURE CAREER', 'LECTURE WEBSITE', 'LECTURE UNITS OF CREDIT',
           'LECTURE INTRODUCTION']

def Match(keyword):
    column = []
    other = []
    for i in keyword:
        if i in columns:
            column.append(i)
        else:
            other.append(i)
    if column == []:
        column = columns
    check = False
    match_res = []

    df_col_1 = pd.read_csv(Data, index_col = 'LECTURE NAME')
    for row in df_col_1.index:
        if row in other:
            for c in df_col_1:
                if c in column:
                    if df_col_1.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_1.loc[row, c]))
                        check = True


    df_col_2 = pd.read_csv(Data, index_col = 'LECTURE CODE')
    for row in df_col_2.index:
        for i in other:
            if row[4:] in i and row[:4] in i:
                for c in df_col_2:
                    if c in column:
                        if df_col_2.loc[row, c] != 'None':
                            match_res.append(message(c, df_col_2.loc[row, c]))
                            check = True
            elif row[4:] in i and row[:4] not in i:
                mess = 'Are you looking for ' + row + '?'
                match_res.append(mess)
                for c in df_col_2:
                    if c in column:
                        if df_col_2.loc[row, c] != 'None':
                            match_res.append(message(c, df_col_2.loc[row, c]))
                            check = True
            else:
                continue
#        if row in other:
#            for c in df_col_2:
#                if c in column:
#                    if df_col_2.loc[row, c] != 'None':
#                        match_res.append(message(c, df_col_2.loc[row, c]))
#                        check = True


    df_col_3 = pd.read_csv(Data, index_col = 'LECTURE LOCATION AND TIME')
    for row in df_col_3.index:
        if row in other:
            for c in df_col_3:
                if c in column:
                    if df_col_3.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_3.loc[row, c]))
                        check = True


    df_col_4 = pd.read_csv(Data, index_col = 'PROFESSOR')
    for row in df_col_4.index:
        if row in other:
            for c in df_col_4:
                if c in column:
                    if df_col_4.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_4.loc[row, c]))
                        check = True


    df_col_5 = pd.read_csv(Data, index_col = 'PROFESSOR CONTACT')
    for row in df_col_5.index:
        if row in other:
            for c in df_col_5:
                if c in column:
                    if df_col_5.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_5.loc[row, c]))
                        check = True


    df_col_6 = pd.read_csv(Data, index_col = 'LECTURE CAREER')
    for row in df_col_6.index:
        if row in other:
            for c in df_col_6:
                if c in column:
                    if df_col_6.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_6.loc[row, c]))
                        check = True

    df_col_7 = pd.read_csv(Data, index_col = 'LECTURE WEBSITE')
    for row in df_col_7.index:
        if row in other:
            for c in df_col_7:
                if c in column:
                    if df_col_7.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_7.loc[row, c]))
                        check = True

    df_col_8 = pd.read_csv(Data, index_col = 'LECTURE UNITS OF CREDIT')
    for row in df_col_8.index:
        if row in other:
            for c in df_col_8:
                if c in column:
                    if df_col_8.loc[row, c] != 'None':
                        match_res.append(message(c, df_col_8.loc[row, c]))
                        check = True

    df_col_9 = pd.read_csv(Data, index_col = 'LECTURE INTRODUCTION')
    for row in df_col_9.index:
        if row in other:
            for c in df_col_9:
                if df_col_9.loc[row, c] != 'None':
                    match_res.append(message(c, df_col_8.loc[row, c]))
                    check = True
    if check is False:
        return None
        #return print_output([])
    return match_res

def message(c, g):
    res = []
    mes = ['The course name is: ', 'The course code is: ', 'The timetable of course is: ',
           'The professor of this course is: ', 'The contact way of professor is: ',
           'The type of course is: ', 'The homepage of lecture is: ',
           'The credit units of lecture is: ', 'The introduction of lecture is: ']
    for i in range(len(columns)):
        if c == columns[i]:
            if i == 2:
                t = g.replace('/', ' and')
                res.append(mes[i] + t)
                
                break
            else:
                res.append(mes[i] + g)
                break
    
    out = ''
    if len(res) != 0:
        for i in res:
            out += i
    return out
