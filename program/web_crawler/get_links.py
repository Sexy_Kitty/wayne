import requests #import requests model
from bs4 import BeautifulSoup  #import BeautifulSoup model
import os  #import os model

class Courses():

    def __init__(self):  #class initialization
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1'}  #Simulate chrome browser
        self.web_url = 'https://www.engineering.unsw.edu.au/study-with-us/current-students/academic-information/electives/pg-electives'  #The web address to access
        self.folder_path = os.getcwd()  #Set the file directory where the file should be stored

    def get_cour(self):
        print('Start get request')
        r = self.request(self.web_url)
        print('Start getting all a tags')
        all_a = BeautifulSoup(r.text, 'lxml').find_all('a')  #Get all the 'a' tags
        print('Start creating folder')
        self.mkdir(self.folder_path)  #Create folder
        print('Start switching folders')
        os.chdir(self.folder_path)   #Switch the path to the folder created above

        course_dic = {}
        for a in all_a[:-2]: #Loop each tag, get the url of the image in the tag and make a network request, and finally save
            a = str(a)
            if "handbook" in a:
                temp_course = a.split('"')
                #There are three semesters in 2019
                #Hence the current information is unavailable on UNSW website
                if 'current' in temp_course[1]:
                    temp_course[1] = temp_course[1].replace('current', '2018')
                course_dic[temp_course[-1][1:9]] = [temp_course[1]]
            if "timetable" in a:
                temp_course = a.split('"')
                timetable = temp_course[1]
                course_name = str(timetable.split('/')[-1][0:8])
                if course_name in course_dic.keys():
                    if len(course_dic[course_name]) == 1:
                        #There are three semesters in 2019
                        #Hence the current information is unavailable on UNSW website
                        if 'current' in timetable:
                            timetable = timetable.replace('current', '2018')
                        course_dic[course_name].append(timetable)

        self.save_url(course_dic,"CSE course list")
        print('Get links of courselist successfully!')
        print('--------------------------------------------------')
        return("CSE course list")

    def save_url(self, url, name): ##Save url
        file_name = name + '.txt'
        with open(file_name, "w") as file:
            for u in url.keys():
                if len(url[u]) == 2:
                    file.write(u + ' ' + url[u][0] + ' ' + url[u][1] + '\n')

    def request(self, url):  #Return the response of websit
        r = requests.get(url, headers=self.headers)  # Sends a get request like the target url address and returns a response object. There are no headers parameters
        return r

    def mkdir(self, path):  ##This function creates a folder
        path = path.strip()
        isExists = os.path.exists(path)
        if not isExists:
            print('Create a folder named', path)
            os.makedirs(path)
            print('Created successfully!')
        else:
            print(path, ' already exists and is no longer created')


