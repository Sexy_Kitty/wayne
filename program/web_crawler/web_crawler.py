import requests #import requests model
from bs4 import BeautifulSoup  #import BeautifulSoup model
import os  #import os model
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from get_links import *
import csv

class CourseDetails():
    def __init__(self,timetable):  #Class initialization
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1'}  #Simulate chrome browser
        self.web_url = timetable  #The web address to access
        self.folder_path = os.getcwd()  #Set the file directory where the file should be stored

    def get_details(self):
        #Start get request
        req = self.request(self.web_url)
        
        #Start getting course name and code
        name, code = self.get_name_code(req, 'classSearchMinorHeading')

        #Start getting course location and time
        location_time, professor = self.get_location_time_professor(req, 'rowLowlight', 'rowHighlight')
        if name != None:
            name = name.upper()
        if professor != None:
            professor = professor.upper()
        #Start getting professor contact
        if not os.path.exists('professor_contact.csv'):
            contact = 'None'
        else:
            if professor != None:
                contact = self.get_contact(professor, 'professor_contact.csv')
            else:
                contact = 'None'
        #Prepare course detail list
        details = [name, code, location_time, professor, contact, 'postgraduate']
        for d in range(len(details)):
            if details[d] == None:
                details[d] = 'None'    
        return details

    def get_name_code(self, r, label):
        #Course name and code are saved in the class of 'td'
        temp_inf = BeautifulSoup(r.text, 'lxml').find_all('td', class_ = label)
        if not temp_inf:
            return self.web_url, None
        course_code = str(temp_inf).split(' ')[2][-8:]
        temp_name = str(temp_inf).split(' ')[3:]
        course_name = ' '.join(temp_name)[:-6].split('</td>')[0]
        return course_name, course_code

    def get_location_time_professor(self, r, label_1, label_2):
        #Course location, tim and professor are saved in the class of 'tr'
        temp_inf_1 = BeautifulSoup(r.text, 'lxml').find_all('tr', class_ = label_1)
        temp_inf_2 = BeautifulSoup(r.text, 'lxml').find_all('tr', class_ = label_2)
        temp_inf = temp_inf_1 + temp_inf_2
        if not temp_inf:
            return None, None
        for t in temp_inf:
            t_ = str(t).split('\n')
            #Only collect the lecture information
            if 'Lecture' in t_[1] and 'WEB' not in t_[4]:
                break
            if 'Seminar' in t_[1] and 'WEB' not in t_[4]:
                break
        days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        temp_time = t_[-2].strip('<td class="data">').strip('</td>').split(' ')
        time_table = []
        for tt in range(len(temp_time)-1,-1,-1):
            if temp_time[tt] in days:
                time_table.insert(0,temp_time[tt:])
                temp_time = temp_time[:tt]
            if not temp_time:
                break

        time_and_location = [' '.join(t_t) for t_t in time_table]
        if not time_and_location:
            return None, None
        n = 0
        professor = ''
        for time in time_table:
            for ti in temp_inf:
                ti_ = str(ti).split('\n')
                if time[0] in ti_[1] and ' '.join(time[1:4]) in ti_[2]:
                    if professor == '':
                        professor = ti_[5].strip('<td class="data">').strip('</td>')
                        professor = ' '.join(professor.split())
                    location = ti_[3].strip('<td class="data">').strip('</td>')
                    
                    if time_and_location[n][-1] == ',':
                        time_and_location[n] = time_and_location[n][:-1]
                    time_and_location[n] += (',' + location)
                    n += 1
                if n >= len(time_and_location):
                    return '/'.join(time_and_location), professor

        return '/'.join(time_and_location).strip(',( )'), professor          

    def get_contact(self, name, contact_file):
        cf = csv.reader(open(contact_file, 'r'))
        for cf_ in cf:
            if name == cf_[0]:
                return cf_[1]
        return 'None'

    def request(self, url):  #Return the response of websit
        r = requests.get(url, headers=self.headers)  # Sends a get request like the target url address and returns a response object. There are no headers parameters
        return r

courses = Courses()  #Create an instance of a class
filename = courses.get_cour()  #Execution method in class
c_dict = {}
#Read prepared course links for web crawler
print('Start web crawler for courses')
with open(filename+'.txt') as file:
    for line in file:
        l = line.split(' ')
        c = CourseDetails(l[2].strip('\n'))
        detail = c.get_details()
        course_code = l[0]
        if course_code == None:
            continue
        if course_code == detail[1]:
            c_dict[course_code] = detail + [l[1], '6 units', 'None']
print('Course dictionary completed')
print('--------------------------------------------------')

#Save in csv file
print('Save in csv file')
with open('dataset.csv','w',newline='') as cf:
    writer = csv.writer(cf)
    writer.writerow(['LECTURE NAME', 'LECTURE CODE', 'LECTURE LOCATION AND TIME',
                     'PROFESSOR', 'PROFESSOR CONTACT', 'LECTURE CAREER',
                     'LECTURE WEBSITE', 'LECTURE UNITS OF CREDIT', 'LECTURE INTRODUCTION'])
    writer.writerows(c_dict.values())
print('Web crawler completed!')
