# -*- coding: utf-8 -*-
import nltk
from nltk.probability import FreqDist
from nltk.corpus import wordnet as wn
import copy

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

columns = ['LECTURE NAME', 'LECTURE CODE', 'LECTURE LOCATION AND TIME',
            'LECTURE CAREER', 'LECTURE WEBSITE', 'LECTURE UNITS OF CREDIT',
           'LECTURE INTRODUCTION','PROFESSOR', 'PROFESSOR CONTACT']
#print(lemma.name for lemma in wn.synset('teacher.n.01').lemmas)

def Similar_words(word):
    similar_word = copy.deepcopy(word)
    for w in word:
        if w in ['CLASS','COURCE','LESSON','SUBJECT']:
            similar_word[word.index(w)] = 'LECTURE'
            continue
        if w in ['LOCATION','PLACE','POSITION','SITUATION','SPOT','WHERE','WHEN','TIME','DATE','PERIOD','SCHEDULE','PLAN','TIMETABLE']:
            similar_word[word.index(w)] = 'LECTURE LOCATION AND TIME'
            continue
        if w in ['WEBSITE','WEB PAGE','SITE','FORUM','NETWORK','INTERNET SITE']:
            similar_word[word.index(w)] = 'LECTURE WEBSITE'
            continue
        if w in ['INTRODUCTION','OUTLINE','OVERVIEW','SUMMARY','ROUGH GUIDE']:
            similar_word[word.index(w)] = 'LECTURE INTRODUCTION'
            continue
        if w in ['ASSISTANT','EDUCATOR','INSTRUCTOR','LECTURER','TEACHER','TUTOR','PROF','WHO']:
            similar_word[word.index(w)] = 'PROFESSOR'
            continue
        if w in ['E-MAIL','LINK','CALL','PHONE']:
            similar_word[word.index(w)] = 'CONTACT'
            continue
    return similar_word

def Combine_words(key_word):
    res = []
    # Combine two words
    if 'LECTURE' in key_word and 'NAME' in key_word:
        res.append('LECTURE NAME')
        key_word.remove('LECTURE')
        key_word.remove('NAME')
    if 'LECTURE' in key_word and 'CODE' in key_word:
        res.append('LECTURE CODE')
        key_word.remove('LECTURE')
        key_word.remove('CODE')
    if 'PROFESSOR' in key_word and 'CONTACT' in key_word:
        res.append('PROFESSOR CONTACT')
        key_word.remove('PROFESSOR')
        key_word.remove('CONTACT')
    return res+key_word
#Similar_words('teacher')

def Transfer_to_noun(verb_word):
    """ Transform a verb to the closest noun: die -> death """
    verb_synsets = wn.synsets(verb_word, pos="v")
    # Word not found
    if not verb_synsets:
        return []
    # Get all verb lemmas of the word
    verb_lemmas = [l for s in verb_synsets \
                   for l in s.lemmas() if s.name().split('.')[1] == 'v']
    # Get related forms
    derivationally_related_forms = [(l, l.derivationally_related_forms()) \
                                    for l in verb_lemmas]
    # filter only the nouns
    related_noun_lemmas = [l for drf in derivationally_related_forms \
                           for l in drf[1] if l.synset().name().split('.')[1] == 'n']
    # Extract the words from the lemmas
    words = [l.name for l in related_noun_lemmas]
    len_words = len(words)
    # Build the result in the form of a list containing tuples (word, probability)
    result = [(w, float(words.count(w))/len_words) for w in set(words)]
    result.sort(key=lambda w: -w[1])
    # return all the possibilities sorted by probability
    return result[0][0]()

def Key_words(sentence):
    sentence_split = nltk.word_tokenize(sentence)
    #print(sentence_split)
    tagged = nltk.pos_tag(sentence_split)
    #print(tagged)
    #fdist1 = FreqDist(sentence_split)
    #minfo = dict(fdist1)

    # Extract noun word and number
    tag_list_n = ['NN','NNS','NNP','NNPS','CD']
    key_word_n = list(set([k.lower() for k,v in tagged if v in tag_list_n]))
    #print(key_word_n)

    # Extract verb word
    tag_list_v = ['VB','VBD','VBG','VBN','VBP','VBZ']
    key_word_v = list(set([k.lower() for k,v in tagged if v in tag_list_v]))
    #print(key_word_v)
    for w in key_word_v:
        if w in ['is','are','be','was','were','been']:
            key_word_v.remove(w)
        if w in ['please']:
            key_word_v.remove(w)
    kwv = copy.deepcopy(key_word_v)
    for w in key_word_v:
        #print(w)
        kwv.append(Transfer_to_noun(w))
        #kwv[key_word_v.index(w)] = Transfer_to_noun(w)
    #print(kwv)

    # Extract WH words
    tag_list_wh = ['WDT','WP','WP$','WRB']
    key_word_wh = list(set([k.lower() for k,v in tagged if v in tag_list_wh]))
    #print(key_word_wh)

    # Total key words
    key_word = key_word_n+kwv+key_word_wh
    #print(key_word)

    # Transfer to Upper word
    u_key_word = []
    #print(key_word)
    for i in key_word:
        if i!=[]:
            u_key_word.append(i.upper())
    #print(u_key_word)

    # Match similar word in attribute table
    key_word = Similar_words(u_key_word)
    #print(key_word)

    # Combine two single word to a word group
    key_word = Combine_words(key_word)
    #print(key_word)

    # Remove same words
    key_word = list(set(key_word))
    #print(key_word)

    return key_word

#print(Key_words("Who teach comp9021"))
#print(Key_words("How to contact the professor of GSOE9820"))
#print(Key_words("When is the lecture of COMP9021?"))
