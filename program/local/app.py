# -*- coding: utf-8 -*-
import os
current_path = os.getcwd()

from input_process import *
from match import *
from flask import Flask, render_template, request
from daily_chat import *

os.chdir(current_path)

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    keywords = Key_words(userText)
    res = Match(keywords)
    if res:
        return '\n'.join(map(str,res))
    else:
        alice_res = Alice_response(userText)
        #print(alice_res)
        return alice_res
        #return '\n'.join(map(str,alice_res))

if __name__ == "__main__":
    app.run()
